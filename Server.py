# Tina Tahvildari
# 9612762752

import hashlib
import socket

def find_data(str):
    data = str.find('data')
    splitted = str[data+4:].split(',')
    ID = splitted[0]
    x = splitted [1]
    y = splitted [2]
    return 'ID = ' + ID + ' , Location = (x= ' + x + ' , y= '+ y +')'

def find_checksum_seq(header_and_data):
    splitted = header_and_data.split('|')
    #header_len = splitted[0]
    seq_num = splitted[1]
    checksum = splitted[2]
    data = splitted[3]
    return checksum , data , seq_num

def calculate_checksum(data):
    checksum = hashlib.sha1(data.encode('utf-8')).hexdigest()
    return checksum

def eof(str):
    is_end = str.find('end')
    if is_end == -1:
        return False
    else:
        return True

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
sock.bind(('127.0.0.1',12345))
full_msg = ''
packet_number = 0
exp_seq = 0
Acknowledge = ''

while True:

    data,addr = sock.recvfrom(500)

    checksum , main_data , seq_num = find_checksum_seq(data.decode())

    s_checksum = calculate_checksum(main_data)

    if checksum == s_checksum and str(exp_seq) == seq_num:
        #print('checksums and sequence numbers matched!')
        if exp_seq == 0 :
            exp_seq = 1
        elif exp_seq ==1:
            exp_seq = 0
        #print('expected = ', str(exp_seq))
        packet_number += 1
        Acknowledge = 'Packet %s Aknowledged With Sequence Number :%s' %(packet_number,seq_num)
        sock.sendto(Acknowledge.encode('utf-8'), addr)

        full_msg += main_data
        if eof(full_msg):
            msg = find_data(full_msg)
            print(msg)
            full_msg = ''
    elif checksum != checksum:
        print('checksums mismatched!')
    elif str(exp_seq) != seq_num:
        # Duplicate packet
        #print('Duplicate Packet. Sending last Ack...')
        sock.sendto(Acknowledge.encode('utf-8'), addr)