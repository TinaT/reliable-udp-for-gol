# Tina Tahvildari
# 9612762752

# This code is
#   based on https://nostarch.com/download/samples/PythonPlayground_sampleCh3.pdf
#   prepared for Computer Networking Class: Ferdowsi University of Mashhad
import hashlib
import random
import secrets
import uuid
import socket

import matplotlib; matplotlib.use("TkAgg")  # For pycharm IDE only
import numpy as np
import matplotlib.pyplot as plt;
import matplotlib.animation as animation


class packet():
    checksum = 0;
    length = 0;
    seqNo = 0;
    msg = 0;

    def make(self, data):
        self.msg = data
        self.checksum=hashlib.sha1(data).hexdigest()
        sc = str(self.seqNo).encode('utf-8') + '|'.encode('utf-8') + self.checksum.encode('utf-8')
        header_len = len(sc)
        header = str(header_len).encode('utf-8') + '|'.encode('utf-8') + sc + '|'.encode('utf-8')
        full_pkt =  header + self.msg
        self.length = str(len(full_pkt))
        #print("Length: %s\nSequence number: %s\nChecksum: %s" % (self.length, self.seqNo,self.checksum))
        return full_pkt


class Agent:
    def __init__(self, id, X, Y,state):
        self.id = id
        self.X = X
        self.Y = Y
        self.state = state

    def check_seq_num(self,seq,server_msg):
        splitted = server_msg.split(':')
        server_seq = splitted[1][0]
        #print(splitted[1][0])
        #print('seq = %s , serv_seq = %s'  %(seq,server_seq))
        if server_seq == str(seq):
            return True
        else:
            return False

    def send_pkt(self,massage,sock , seq):
        pkt = packet()
        pkt.seqNo = seq
        sock.sendto(pkt.make(massage), ('127.0.0.1', 12345))

    def snd_pkt_to_server(self):

        client_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client_sock.settimeout(0.1)

        msg = str(self.id).encode('utf-8') + ','.encode('utf-8')+ str(self.X).encode('utf-8') + ','.encode('utf-8')+ str(self.Y).encode('utf-8') + ','.encode('utf-8')+\
              str(self.state).encode('utf-8')

        n = random.randint(500*8 , 1500*8)
        rand = secrets.randbits(n)
        msg = str(rand).encode('utf-8') + str('data').encode()+ msg + ',end'.encode('utf-8')

        print(len(msg))

        i = 0
        while i <len(msg)//(500-46) :
            if i % 2 ==0:
                seq = 0
            else:
                seq = 1

            self.send_pkt(msg[i * (500 - 46):i * (500 - 46) + (500 - 46)], client_sock , seq)

            try:
                data, addr = client_sock.recvfrom(500)
                print(str(data))
                if not self.check_seq_num(seq, str(data)):
                    while not self.check_seq_num(seq, str(data)):
                        print('Sequence number is wrong!')
                        self.send_pkt(msg[i * (500 - 46):i * (500 - 46) + (500 - 46)], client_sock, seq)
                        data, addr = client_sock.recvfrom(500)
            except:
                print('time out!')
                self.send_pkt(msg[i * (500 - 46):i * (500 - 46) + (500 - 46)], client_sock, seq)
                data, addr = client_sock.recvfrom(500)
            finally:
                if i % 2 == 0:
                    seq = 0
                else:
                    seq = 1
                i += 1

        if len(msg)%(500-46) != 0:
            if i % 2 ==0:
                seq = 0
            else:
                seq = 1
            x = len(msg)%(500-46)
            self.send_pkt(msg[len(msg)-x :len(msg)],client_sock , seq)

            try:
                data, addr = client_sock.recvfrom(500)
                print(str(data))
                #print(self.check_seq_num(seq, str(data)))
                if not self.check_seq_num(seq,str(data)):
                    while not self.check_seq_num(seq, str(data)):
                        print('Sequence number is wrong!')
                        self.send_pkt(self.send_pkt(msg[len(msg)-x :len(msg)],client_sock , seq))
                        data, addr = client_sock.recvfrom(500)
            except:
                print('timeout!')
                self.send_pkt(msg[len(msg) - x:len(msg)], client_sock, seq)
                data, addr = client_sock.recvfrom(500)

        client_sock.close()

def select_random_dead_agent(all_agents):
    dead_agents=[]
    for i in range(len(all_agents)):
        if all_agents[i].state == 0:
            dead_agents.append(i)
    rand = random.randint(0, len(dead_agents)-1)
    return all_agents[dead_agents[rand]]

N = 30  # Grid size is N*N
live = 255
dead = 0
state = [live, dead]

# Create random population (more dead than live):
grid = np.random.choice(state, N * N, p=[0.3, 0.7]).reshape(N, N)
# To learn more about not uniform random visit:
# https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.random.choice.html

def update(data):
    agents = []
    global grid
    temp = grid.copy()
    for i in range(N):
        for j in range(N):
            # Compute 8-neighbor sum
            total = (grid[i, (j - 1) % N] + grid[i, (j + 1) % N] +
                     grid[(i - 1) % N, j] + grid[(i + 1) % N, j] +
                     grid[(i - 1) % N, (j - 1) % N] + grid[(i - 1) % N, (j + 1) % N] +
                     grid[(i + 1) % N, (j - 1) % N] + grid[(i + 1) % N, (j + 1) % N]) / 255
            # Apply Conway's Rules:
            # 1- Any live cell with fewer than two live neighbours dies, as if by underpopulation.
            # 2- Any live cell with two or three live neighbours lives on to the next generation.
            # 3- Any live cell with more than three live neighbours dies, as if by overpopulation.
            # 4- Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
            if grid[i, j] == live:
                if (total < 2) or (total > 3):
                    temp[i, j] = dead
            else:
                if total == 3:
                    temp[i, j] = live
            agent = Agent(uuid.uuid4(), i, j, temp[i, j])
            agents.append(agent)
    selected_agent = select_random_dead_agent(agents)
    selected_agent.snd_pkt_to_server()
    mat.set_data(temp)
    grid = temp
    print('--------')
    return mat


# Animation
fig, ax = plt.subplots()
mat = ax.matshow(grid)
ani = animation.FuncAnimation(fig, update, interval=500)
plt.show()